# Roman numeral Kata

Kata donde consistente en transformar números decimales en romanos y viceversa.
El propósito de la kata es practicar con la metodología TDD.

La implementación se realiza como métodos de extensión de la clase Int.