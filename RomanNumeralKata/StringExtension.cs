﻿namespace RomanNumeralKata
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class StringExtension
    {

        private static readonly Dictionary<char, int> charNumber = new Dictionary<char, int>()
        {
            {'I', 1},
            {'V', 5},
            {'X', 10},
            {'L', 50},
            {'C', 100},
            {'D', 500},
            {'M', 1000}
        };

        public static int RomanToDecimal(this string romanNumber)
        {
            var cleanRomanNumber = CleanNumber(romanNumber);
            ValidateInput(cleanRomanNumber);
            return ProccessListOfRomanDigits(cleanRomanNumber.ToList());
        }

        private static string CleanNumber(string romanNumber)
        {
            return romanNumber?.ToUpper().Trim();
        }

        private static void ValidateInput(string romanNumber)
        {
            var romanDigitList = string.Join("", charNumber.Keys);
            var hasInvalidRomanDigit = romanNumber?.ToCharArray().Any(c => !romanDigitList.Contains(c)) ?? true;
            if (string.IsNullOrEmpty(romanNumber))
                throw new ArgumentException(nameof(romanNumber), "No se ha proporcionado ningún número para convertir.");

            if (hasInvalidRomanDigit)
                throw new ArgumentException(nameof(romanNumber), "El texto no es un número romano válido.");
        }

        private static int charToRoman(char c)
        {
            return charNumber[c];
        }

        private static int ProccessListOfRomanDigits(IList<char> romanDigits)
        {
            //Invertimos la lista y convertimos los caracteres romanos a números.
            var invertedCharToNumber = romanDigits.Reverse().Select(charToRoman).ToList();

            var result = 0;
            var previous = 0;

            foreach (var actual in invertedCharToNumber)
            {
                result += AddDigitToNumber(actual, previous);

                previous = actual;
            }

            return result;
        }

        private static int AddDigitToNumber(int digitToAdd, int previousDigit)
        {
            if (digitToAdd < previousDigit)
            {
                return -1 * digitToAdd;
            }
            else
            {
                return digitToAdd;
            }
        }
    }
}