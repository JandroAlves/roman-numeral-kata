﻿namespace RomanNumeralKata
{
    using System;
    using System.Collections.Generic;

    public static class IntegerExtension
    {  
        private static readonly Dictionary<int, char> NumberChar = new Dictionary<int, char>()
        {
            {1, 'I'},
            {5, 'V'},
            {10, 'X'},
            {50, 'L'},
            {100, 'C'},
            {500, 'D'},
            {1000, 'M'},
            {5000, ' '},
            {10000, ' '}
        }; 

        public static string ToRoman(this int number)
        {
            ValidateInput(number);

            var result = string.Empty;
            for (var baseNumber = 1000; baseNumber > 0; baseNumber /= 10)
            {
                var digit = number/baseNumber;
                if (digit == 0) continue;
                
                result += GetRomanOff(digit, NumberChar[baseNumber], NumberChar[baseNumber*5], NumberChar[baseNumber*10]);
                number -= digit * baseNumber;
            }

            return result;
        }

        private static string GetRomanOff(int digit, char baseUnit, char fiveTimesUnit, char tenTimesUnit)
        {
            if (digit <= 3)
            {
                return new string(baseUnit, digit);
            }
            if (digit < 5)
            {
                return $"{baseUnit}{fiveTimesUnit}";
            }
            if (digit < 9)
            {
                return fiveTimesUnit + new string(baseUnit, digit - 5);
            }
            return $"{baseUnit}{tenTimesUnit}";
        }

        private static void ValidateInput(int number)
        {
            if (number == 0) throw new ArgumentOutOfRangeException(nameof(number), "El cero no existe en números romanos.");
            if (number < 0) throw new ArgumentOutOfRangeException(nameof(number), "No hay números romanos negativos.");
            if (number >= 4000) throw new ArgumentOutOfRangeException(nameof(number), "No se convertir números mayores de 3999");
        }
    }
}
