﻿using Xunit;

namespace Tests
{
    using System;
    using RomanNumeralKata;

    public class ToRomanTests
    {
        [Fact]
        public void cero_excepcion()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => 0.ToRoman());
        }
        [Fact]
        public void negativo_excepcion()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => (-27).ToRoman());
        }
        [Fact]
        public void mayor_que_3999_excepcion()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => 4765.ToRoman());
        }

        [Fact]
        public void uno_igual_I()
        {
            Assert.Equal("I", 1.ToRoman());
        }

        [Fact]
        public void cinco_igual_V()
        {
            Assert.Equal("V", 5.ToRoman());
        }

        [Fact]
        public void diez_igual_X()
        {
            Assert.Equal("X", 10.ToRoman());
        }

        [Fact]
        public void cincuenta_igual_L()
        {
            Assert.Equal("L", 50.ToRoman());
        }

        [Fact]
        public void cien_igual_C()
        {
            Assert.Equal("C", 100.ToRoman());
        }

        [Fact]
        public void dos_igual_II()
        {
            Assert.Equal("II", 2.ToRoman());
        }

        [Fact]
        public void tres_igual_III()
        {
            Assert.Equal("III", 3.ToRoman());
        }

        [Fact]
        public void cuatro_igual_IV()
        {
            Assert.Equal("IV", 4.ToRoman());
        }

        [Fact]
        public void seis_igual_VI()
        {
            Assert.Equal("VI", 6.ToRoman());
        }
        [Fact]
        public void siete_igual_VII()
        {
            Assert.Equal("VII", 7.ToRoman());
        }
        [Fact]
        public void ocho_igual_VIII()
        {
            Assert.Equal("VIII", 8.ToRoman());
        }

        [Fact]
        public void nueve_igual_IX()
        {
            Assert.Equal("IX", 9.ToRoman());
        }

        [Fact]
        public void doce_igual_XII()
        {
            Assert.Equal("XII", 12.ToRoman());
        }

        [Fact]
        public void catorce_igual_XIV()
        {
            Assert.Equal("XIV", 14.ToRoman());
        }

        [Fact]
        public void quince_igual_XV()
        {
            Assert.Equal("XV", 15.ToRoman());
        }

        [Fact]
        public void dieciocho_igual_XVIII()
        {
            Assert.Equal("XVIII", 18.ToRoman());
        }

        [Fact]
        public void diecinueve_igual_XIX()
        {
            Assert.Equal("XIX", 19.ToRoman());
        }

        [Fact]
        public void veinte_igual_XX()
        {
            Assert.Equal("XX", 20.ToRoman());
        }

        [Fact]
        public void treintayocho_igual_XXXVIII()
        {
            Assert.Equal("XXXVIII", 38.ToRoman());
        }

        [Fact]
        public void cuarentaynueve_igual_XLIX()
        {
            Assert.Equal("XLIX", 49.ToRoman());
        }

        [Fact]
        public void sesentaytres_igual_LXIII()
        {
            Assert.Equal("LXIII", 63.ToRoman());
        }

        [Fact]
        public void ochentaycuatro_igual_LXXXIV()
        {
            Assert.Equal("LXXXIV", 84.ToRoman());
        }

        [Fact]
        public void noventaynueve_igual_XCIX()
        {
            Assert.Equal("XCIX", 99.ToRoman());
        }

        [Fact]
        public void cientocincuentaycuatro_igula_CLIV()
        {
            Assert.Equal("CLIV", 154.ToRoman());
        }

        [Fact]
        public void ochoncientosveintiocho_igula_DCCCXXVIII()
        {
            Assert.Equal("DCCCXXVIII", 828.ToRoman());
        }

        [Fact]
        public void tresmilochocientosochentayocho_igula_MMMDCCCLXXXVIII()
        {
            Assert.Equal("MMMDCCCLXXXVIII", 3888.ToRoman());
        }
    }
}
