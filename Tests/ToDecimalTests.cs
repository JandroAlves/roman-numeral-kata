﻿namespace Tests
{
    using System;
    using RomanNumeralKata;
    using Xunit;

    public class ToDecimalTests
    {

        [Fact]
        public void I_igual_1()
        {
            Assert.Equal(1, "I".RomanToDecimal());
        }
        [Fact]
        public void V_igual_5()
        {
            Assert.Equal(5, "V".RomanToDecimal());
        }
        [Fact]
        public void X_igual_10()
        {
            Assert.Equal(10, "X".RomanToDecimal());
        }
        public void L_igual_50()
        {
            Assert.Equal(50, "L".RomanToDecimal());
        }
        public void C_igual_100()
        {
            Assert.Equal(100, "C".RomanToDecimal());
        }
        public void D_igual_500()
        {
            Assert.Equal(500, "D".RomanToDecimal());
        }
        public void M_igual_1000()
        { Assert.Equal(1000, "M".RomanToDecimal()); }

        [Fact]
        public void caracter_no_es_romano_excepcion()
        {
            Assert.Throws<ArgumentException>(() => "XFP".RomanToDecimal());
        }

        [Fact]
        public void vacio_no_es_romano_excepcion()
        {
            Assert.Throws<ArgumentException>(() => "".RomanToDecimal());
        }

        [Fact]
        public void III_igual_3()
        {
            Assert.Equal(3, "III".RomanToDecimal());
        }

        [Fact]
        public void IV_igual_4()
        {
            Assert.Equal(4, "IV".RomanToDecimal());
        }

        [Fact]
        public void VI_igual_6()
        {
            Assert.Equal(6, "VI".RomanToDecimal());
        }

        [Fact]
        public void IX_igual_9()
        {
            Assert.Equal(9, "IX".RomanToDecimal());
        }

        [Fact]
        public void XV_igual_15()
        {
            Assert.Equal(15, "XV".RomanToDecimal());
        }

        [Fact]
        public void XLVII_igual_47()
        {
            Assert.Equal(47, "XLVII".RomanToDecimal());
        }

        [Fact]
        public void XCIX_igual_99()
        {
            Assert.Equal(99, "XCIX".RomanToDecimal());
        }

        [Fact]
        public void CLXXIV_igual_174()
        {
            Assert.Equal(174, "CLXXIV".RomanToDecimal());
        }

        [Fact]
        public void DCLXXXIX_igual_689()
        {
            Assert.Equal(689, "DCLXXXIX".RomanToDecimal());
        }

        [Fact]
        public void MMXXI_igual_2021()
        {
            Assert.Equal(2021, "MMXXI".RomanToDecimal());
        }
    }
}